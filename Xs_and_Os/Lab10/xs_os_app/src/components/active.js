import React from "react"
import '../App.css'
import ToggleButton from "./toggle_button"; 
import RowActiv from "./rows_active";

class Activ extends React.Component 
{
    constructor(props) {
        super(props);
        this.state = {
            checked: false,
        }
    }
    render() {
        const { checked } = this.state;
        return (
            <body>   
                <div id="active_players">     
                    <div id="active_header">
                        <h2>Активные игроки</h2>
                        <div className="switchof_activeplayers">
                            Только свободные &nbsp;&nbsp;   
                            <ToggleButton onChange={(value) => this.setState({checked: value})} checked={this.state.checked}/>
                        </div>
                    </div>     
                    <div id="tableof_activeplayers">
                        <RowActiv player='Александров Игнат Анатолиевич' show={!checked}/>
                        <RowActiv player='Василенко Эрик Платонович' show={!checked}/>
                        <RowActiv player='Быков Юрий Виталиевич' show={!checked}/>
                        <RowActiv player='Галкин Феликс Платонович' show={!checked}/>
                        <RowActiv player='Комаров Цефас Александрович' show={!checked}/>
                        <RowActiv player='Шевченко Рафаил Михайлович' show={!checked}/>
                        <RowActiv player='Гордеев Шамиль Леонидович' show={!checked}/>
                        <RowActiv player='Бобров Фёдор Викторович' show={!checked}/>
                        <RowActiv player='Суворов Феликс Григорьевич' show={!checked}/>
                        <RowActiv player='Марков Йошка Фёдорович' show={!checked}/>
                    </div>
                </div>    
            </body>
        )
    } 
}

export default Activ;