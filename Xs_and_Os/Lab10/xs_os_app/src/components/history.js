import React, { useState } from "react";
import "../App.css";
import img_x from "./imgs/x.svg";
import img_o from "./imgs/zero.svg";
import img_trophy from "./imgs/trophy-mini.svg";
import DataHistory_Games from "./data_historygames";

const History_Games = (props) => {
  const [games, setPlayers] = useState(DataHistory_Games);
  return (
    <div id="list_games">
      <div id="active_header" style={{width: '100%', height: '45px'}}>
        <h2>История игр</h2>
      </div>
      <div id="tableof_activeplayers">
        <div className="header_listgames">
          <th className="first_column">
            Игроки
          </th>
            <div style={{display:'flex', gap:'20px'}}>
              <th className="middle_columns">
                Дата
              </th>
              <th className="middle_columns" style={{width: '145px'}}>
                Время игры
              </th>
            </div>
        </div>
      </div>
      {games.map((game) => (          
        <tr className="row_listgames" key={game.id}>
          <div className="rivals">
            <td className="players_columns">
              <img className="players_role" src={img_o}/>{game.player_O}
              <img src={game.winner === 'O' ? img_trophy : null}/>
            </td>
            против
            <td className="players_columns">
              <img className="players_role" src={img_x}/>{game.player_X}
              <img src={game.winner === 'X' ? img_trophy : null}/>
            </td>
          </div>
            <td className="middle_col">{game.date_game}</td>
            <td className="middle_col" style={{width: '140px'}}>{game.time_game}</td>
        </tr>
        ))}    
    </div>
  );
};

export default History_Games;