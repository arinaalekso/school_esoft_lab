import React from "react"
import '../App.css'

function Top() 
{
    return (
        <div className="body_rating">
            <div id="rating"> 
                <h1 id="h1_rating">Рейтинг игроков</h1>
                <table>
                    <tr>
                        <th className="col_first">ФИО</th>
                        <th className="col_middle">Всего игр</th>
                        <th className="col_middle">Победы</th>
                        <th className="col_middle">Проигрыши</th>
                        <th className="col_last">Процент побед</th>
                    </tr>
                    <tr>
                        <td className="col_first">Александров Игнат Анатолиевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Шевченко Рафаил Михайлович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Мазайло Трофим Артёмович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Логинов Остин Данилович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Борисов Йошка Васильевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Соловьёв Ждан Михайлович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Негода Михаил Эдуардович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Гордеев Шамиль Леонидович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Многогрешный Павел Виталиевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Александров Игнат Анатолиевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Волков Эрик Алексеевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Кузьмин Ростислав Васильевич</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Стрелков Филипп Борисович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                    <tr>
                        <td className="col_first">Галкин Феликс Платонович</td>
                        <td className="col_middle">24534</td>
                        <td className="col_green">9824</td>
                        <td className="col_red">1222</td>
                        <td className="col_middle">87%</td>
                    </tr>
                </table>
            </div>
        </div>
    )
}

export default Top;