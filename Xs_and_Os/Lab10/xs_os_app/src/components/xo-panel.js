import React from "react"
import {observer} from "mobx-react"
import '../App.css'
import img_x from "./imgs/x.svg"
import img_zero from "./imgs/zero.svg" 
import ModalWindow from "./modal"
import GameArchive from "../archive/game_archive"
import ChatMessages from "./mockup_chats_message"

class XOpanel extends React.Component 
{
    render(){
        let status;
        if (GameArchive.winner_player!=null) 
        {
            status = <div id="game-step">Победитель: &nbsp;{(GameArchive.winner_player === 'X' ? <img src={img_x}/>: <img src={img_zero}/>)} &nbsp;{(GameArchive.NextMoveX ? 'Владелен Пупкин' : 'Екатерина Плюшкина' )}</div> 
        }
        else
        {
            status = <div id="game-step">Ходит&nbsp;{(GameArchive.NextMoveX ? <img src={img_x}/>: <img src={img_zero}/>)} &nbsp;{(GameArchive.NextMoveX ? 'Екатерина Плюшкина':'Владелен Пупкин' )}</div>
        }
        let winner_user;
        if (GameArchive.winner_player === 'X') 
        {
            winner_user = GameArchive.Move_X+"12"
        }
        if (GameArchive.winner_player === 'O') 
        {
            winner_user = GameArchive.Move_O+"1"
        } 
        if (GameArchive.winner_player === 'Ничья') 
        {
            winner_user = 'Ничья'
        } 
        return (
        <div id="main-container">
            <div id="subject-list">
                <h1>Игроки</h1>
                <div class="container">
                    <div class="subject-container">
                        <div>
                            <img class="subject-icon" src={img_zero}/>
                        </div>
                        <div class="subject-info">                                         
                            <div class="subject-name"> {GameArchive.FullMove_O} </div>
                            <div class="subject-percent"> {GameArchive.AmountWin_O} </div>
                        </div>
                    </div>
                    <div class="subject-container">
                        <div>
                            <img class="subject-icon" src={img_x} />
                        </div>
                        <div class="subject-info">
                            <div class="subject-name">
                                {GameArchive.FullMove_X}
                            </div>
                            <div class="subject-percent">
                                {GameArchive.AmountWin_X}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="game-container">
                <div id="game-time">
                    {GameArchive.formattedTime}
                </div>
                <div id="game-board">
                    {GameArchive.renderGB(0)}
                    {GameArchive.renderGB(1)}
                    {GameArchive.renderGB(2)}
                    {GameArchive.renderGB(3)}
                    {GameArchive.renderGB(4)}
                    {GameArchive.renderGB(5)}
                    {GameArchive.renderGB(6)}
                    {GameArchive.renderGB(7)}
                    {GameArchive.renderGB(8)}
                    {console.info(GameArchive.winner_player)}
                    {GameArchive.winner_player == null ? null : <ModalWindow winner_userplayer = {winner_user} showModal = {true} />}
                </div>
                {status}
            </div>
            <div>
                <ChatMessages/>
            </div>
        </div>)
    }
}

export default observer(XOpanel);