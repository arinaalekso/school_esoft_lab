import React from 'react';
import { observer } from 'mobx-react';
import img_trophy from './imgs/trophy.svg';
import ModalArchive from '../archive/modal_archive';
import GameArchive from '../archive/game_archive';

const ModalWindow = observer((props) => {
  if (!ModalArchive.showModalWindows) 
  {
    return null;
  }
  const closeModal = () => {ModalArchive.closeModalWindows();};
  let win_mess="";
  let win_img="";
  if (props.winner_userplayer!=="Ничья")
  {
    win_img=<img src={img_trophy}/>;
    win_mess=<h2>{props.winner_userplayer} победил!</h2>;
  }
  else
  {
    win_mess=<h2> Ничья! </h2> ;
  }

  return (
    <div className='modal_back'>
      <div className="popup_modalwindow">
        {props.children}
        {win_img}
        {win_mess}
        <button className="open_newgame" onClick={GameArchive.FactoryDefault} style={{ backgroundColor: '#60C2AA' }}>
          {console.log(GameArchive)}
          Новая игра
        </button>
        <button className="close_modalwin" onClick={closeModal} style={{ backgroundColor: '#F7F7F7', color: '#373745' }}>
          Закрыть окно
        </button>
      </div>
    </div>
  );
});

export default ModalWindow;