import React, {useState} from 'react'; 
import PropTypes from "prop-types";

class ToggleButton extends React.Component 
{
    render() {
        return (
            <label className='label_toggle'>
                <input type="checkbox" className='togglebtn' checked={this.props.checked} onChange ={(e) => this.props.onChange(e.target.checked)} />
                <div className='circle_toggle point'></div>
            </label>
        );
    }
}

ToggleButton.propTypes = {
    checked: PropTypes.bool,
    onChange: PropTypes.func,
}

export default ToggleButton;