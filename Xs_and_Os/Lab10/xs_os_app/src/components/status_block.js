import React from 'react';

const StatusBlock = (props) => {
  if (!props.blocked) 
  {
    return(
      <div className="status_blocked">
        <button>Заблокирован</button>
      </div>
    )
  }  
  return(
    <div className="status_active">
      <button>Активен</button>
    </div>
  );
};

export default StatusBlock;