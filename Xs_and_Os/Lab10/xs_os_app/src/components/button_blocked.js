import React, { useState } from 'react';
import img_block from "./imgs/block.svg";

const ButtonBlock = (props) => {
  const [blocked, setBlocked] = useState(props.blocked);
  const ChangingBlockingStatus = () => {
    setBlocked(!blocked);
    props.onStatusChang();
  };  
  if (!blocked) 
  {
    return (
      <div className="status_unblockbtn">
        <button onClick={ChangingBlockingStatus}>Разблокировать</button>
      </div>
    )
  }    
  return (
    <div className="status_blockbtn">
      <button className='button_blockbtn' onClick={ChangingBlockingStatus}><img className='img_block' src={img_block} /> Заблокировать</button>
    </div>
    );
};

export default ButtonBlock;