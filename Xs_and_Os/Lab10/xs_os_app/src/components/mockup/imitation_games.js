import React from "react";
import Players from "../players";
import ImitatApiCall from "./imitation_call_api";

const ImitatApiGame = {
    getPlayersInfo: async () => {
        let confirmation = true;
        while (confirmation) 
        {
            try 
            {
                const answer = await ImitatApiCall(Players);
                confirmation = !confirmation; 
                return answer;
            } 
            catch (error) 
            {
                console.error(error,"Произошла ошибка загрузки информации о игроках. Запрос повторно отправляется на сервер."); 
            }
        }
    },
    sendIsNextMove: (status) => { localStorage.setItem("IsNextMove", JSON.stringify(status));},
    getisNextMove: async () => {
        let confirmation = true;
        while (confirmation) 
        {
            try 
            {
                const IsNextMove = await localStorage.getItem("IsNextMove");
                const answer = ImitatApiCall(IsNextMove);
                confirmation = !confirmation;
                return answer;
            } 
            catch (error) 
            {
                console.error(error,"Произошла ошибка загрузки статуса хода. Запрос повторно отправляется на сервер. Повторяем запрос.");
            }
        }
    },
    sendFieldState: (field) => {localStorage.setItem("thisfield", JSON.stringify(field));},
    getFieldState:async() => {
        let confirmation = true;
        while (confirmation) 
        {
            try 
            {
                const thisfield = await localStorage.getItem("thisfield");
                const answer =  ImitatApiCall(thisfield);
                confirmation = !confirmation;
                return answer;
            }
            catch (error) 
            {
                console.error(error,"Произошла ошибка загрузки игрового поля. Запрос повторно отправляется на сервер."); 
            }
        }
    },
    sendChatMessage: (message) => {localStorage.setItem("chat", JSON.stringify(message));},
    getChatMessage: async () => {
        let confirmation = true;
        while (confirmation) 
        {
            try 
            {
                const messages = await localStorage.getItem("chat");
                const answer = ImitatApiCall(messages);
                confirmation = !confirmation; 
                return answer;
            } 
            catch (error) 
            {
                console.error(error, "Произошла ошибка загрузки чата. Запрос повторно отправляется на сервер."); 
            }
        }
    },
};

export default ImitatApiGame;