//import React from "react";
import ImitatApiCall from "./imitation_call_api";

class Controllers //extends React.Component
{
    constructor(){
        this.searching = {};
    }
    emit(type, involved) 
    {
        if (this.searching[type]) 
        {
            this.searching[type].forEach((cb) => cb(type, involved, this));
        }
    }
    on(type, cb) 
    {
        if (!this.searching[type]) 
        {
            this.searching[type] = [];
        }
        this.searching[type].push(cb);
    }
}
const response="Ответ сервера";
class GamesMockUp 
{    
    constructor(){
        this.controller = new Controllers();
        this.controller.on('send-message', this.onSendMessage.bind(this));
        this.controller.on('message-sent', this.onMessageSent.bind(this));
        this.controller.on('make-move', this.onMakeMove.bind(this));
        this.controller.on('move-made', this.onMoveMade.bind(this));
        this.controller.on('move-failed', this.onMoveFailed.bind(this));
        this.controller.on('error', this.onError.bind(this));
    }
    ProcessingFailure(error, reason) 
    {
        this.controller.emit('error', {error, reason});
    }
    onSendMessage(userId, text) 
    {
        console.info('Сервер получил сообщение от '+userId);
        try 
        {
            ImitatApiCall();
            this.controller.emit('send-message', {userId, text});
        }
        catch (error) 
        {
            this.ProcessingFailure('error',"Произошла ошибка отправки сообщения.")
        }
    }
    onMessageSent(userId, text)
    {
        console.info(userId+"отправил сообщение:"+text,response);
    }
    onMakeMove(type, involved) 
    {
        if (!involved.cell) 
        {
            this.controller.emit('move-made', {userId: involved.userId, row: involved.row, col: involved.col});
        } 
        else 
        {
            this.controller.emit('move-failed', {});
        }
    }
    onMoveMade(type, involved) 
    {
        console.info(involved.userId+"сделал ход"+involved.row+":"+involved.col,response);
    }
    onMoveFailed() 
    {
        alert("Ход невозможен!");
    }
    onError(error, reason)
    {
        console.error(error, reason,response);
    }
    MakeaMove(userId, row, col, cell) 
    {
        console.info(userId+"сделал ход"+row+":"+col,"Сообщение серверу");
        try 
        {
            ImitatApiCall();
            this.controller.emit('make-move', {userId, row, col, cell});
        }
        catch (error) 
        {
            this.ProcessingFailure(error, "Ошибка хода.")
        }
    }
}

const GamesMockUpkWS = new GamesMockUp();
export default GamesMockUpkWS;