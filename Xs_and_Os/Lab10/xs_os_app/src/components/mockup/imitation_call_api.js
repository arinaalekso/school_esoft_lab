import React from "react";

const ImitatApiCall = (el) => new Promise((resolve, reject)=> {
    setTimeout(() => {
        if (Math.random() < 0.1)
        {
            reject(new Error());
        } 
        else 
        {
            resolve(el);
        }
    }, Math.random()*3000)
});

export default ImitatApiCall;