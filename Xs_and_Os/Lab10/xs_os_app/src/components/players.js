const Players = [
    { 
      id:1, 
      name: 'Александров Игнат Анатолиевич', 
      age: 24,  
      gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021',
      amountWin: 0.23
    },
    { 
      id:2, 
      name: 'Мартынов Остап Фёдорович', 
      age: 12,  
      gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021',
      amountWin: 0.67
    },
    { 
      id:3, 
      name: 'Комаров Цефас Александрович', 
      age: 83,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:4, 
      name: 'Кулаков Станислав Петрович', 
      age: 43,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:5, 
      name: 'Борисов Йошка Васильевич', 
      age: 32,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:6, 
      name: 'Негода Михаил Эдуардович', 
      age: 33,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:7, 
      name: 'Жданов Зураб Алексеевич', 
      age: 24,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:8, 
      name: 'Бобров Фёдор Викторович', 
      age: 19,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    {
      id:9, 
      name: 'Многогрешный Павел Виталиевич', 
      age: 24,  
      gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
  ]  

export default Players;