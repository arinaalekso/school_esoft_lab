import React, {useEffect } from 'react';
import img_send_btn from "./imgs/send-btn.svg";
import GameArchive from '../archive/game_archive';
import GamesMockUpkWS from './mockup/ws_mockup';
import '../App.css'

const Messages = (
    {
        name_player, 
        X_or_O, 
        time_message, 
        text_message,
    }
    ) => {
    let class_msg = "";
    let class_sbj = "";
    function IdentifySide () 
    {
        if (X_or_O === "X") 
        {
            class_msg = "msg-container other";
            class_sbj = "subject-name x";
        }
        else if (X_or_O === "O") 
        {
            class_msg = "msg-container me";
            class_sbj = "subject-name zero";
        }
    }
    IdentifySide();
    return (
        <div className={class_msg}>
            <div className="msg-header">
                <div className={class_sbj}>
                    {name_player}
                </div>
                <div className="time">
                    {time_message}
                </div>
            </div>
            <div className="msg-body">
                {text_message}
            </div>
        </div>
    );
};

const ChatMessages = () => {
    useEffect(() => {
        const msgs_container = document.querySelector(".msgs-container");
        msgs_container.scrollTop = msgs_container.scrollHeight;
        }, [GameArchive.chat_games]);
    useEffect(() => {
        if (GameArchive.chat_games.length === 0) 
            return;

        const msgs_container = document.querySelector(".msgs-container");
        const top_div = document.querySelector(".disappear-up");
        const bottom_div = document.querySelector(".disappear-down");
        const scrollable = () => {
            if (msgs_container.scrollTop === 0) 
            {
                top_div.classList.add("unseen");
            } 
            else 
            {
                top_div.classList.remove("unseen");
            }
            if (msgs_container.scrollTop >= (msgs_container.scrollHeight - msgs_container.clientHeight)) 
            {
                bottom_div.classList.add("unseen");
            } 
            else 
            {
                bottom_div.classList.remove("unseen");
            }
        };
        const processWindowSizeChanges = () => { 
            if (msgs_container.scrollHeight <= msgs_container.clientHeight) 
            {
                top_div.classList.add("unseen");
                bottom_div.classList.add("unseen");
            } 
            else
            {
                top_div.classList.remove("unseen");
                bottom_div.classList.remove("unseen");
            }
        };
        msgs_container.addEventListener("scroll", scrollable);
        window.addEventListener("resize", processWindowSizeChanges);
        processWindowSizeChanges();
        return () => {
            msgs_container.removeEventListener("scroll", scrollable);
            window.removeEventListener("resize", processWindowSizeChanges);
        };
    }, [GameArchive.chat_games.length]);
    

    let ActualPlayer = null;
    let PlayersName = null;
    if (GameArchive.NextMoveX) 
    {
        ActualPlayer = GameArchive.player_X;
        PlayersName = GameArchive.Move_X;
    }
    else 
    {
        ActualPlayer = GameArchive.player_O;
        PlayersName = GameArchive.Move_O;
    }
    const PlayersSide = ActualPlayer?.side;
    const OnSendMessage = () => {
        const message_text = document.getElementById("message").value;
        const time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
        const new_messagetext = {
        time: time,
        name: PlayersName,
        side: PlayersSide,
        text: message_text
        };
        if (message_text) 
        {
            GameArchive.setChatMessages([...GameArchive.chat_games, new_messagetext]);
            document.getElementById("message").value = "";
            alert(GameArchive.player_O);
        }
        GamesMockUpkWS.onSendMessage(new_messagetext.name,new_messagetext.text);
        alert(new_messagetext.text);
    };
    function displayMessage(info_data) 
    {
        return info_data.map(item => (<Messages X_or_O={item.side} name_player={item.name} time_message={item.time} text_message={item.text} />))
    }
    return (
        <div id="chat-container">
            <div className="msgs-container">
                {GameArchive.chat_games.length === 0 ? ( 
                <div className="no-messages">Сообщений еще нет</div>
                ) : (
                <>
                    <div className="disappear-up" />
                    {displayMessage(GameArchive.chat_games)}
                    <div className="disappear-down" />
                </>
                )}
            </div>
            <div className="msg-interactive-elements">
                <textarea placeholder="Сообщение.." id="message">
                </textarea>
                <button>
                    <img src={img_send_btn} onClick={OnSendMessage} />
                </button>
            </div>
        </div>
    );
};

export default ChatMessages;