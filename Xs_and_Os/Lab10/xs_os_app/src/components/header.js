import React from "react"
import {BrowserRouter as Router,Routes, Route, Link} from "react-router-dom"
import '../App.css'
import img_logo from "./imgs/s-logo.svg"
import img_signout from "./imgs/signout-icon.svg"
import Active from "./active"
import Panel_XO from "./xo-panel"
import Top from "./top"
import List_Players from "./list_players"
import History_Games from "./history"
import SessionArchive from "../archive/session_archive" 

function Header() 
{
    const [state, setState] = React.useState({ active_element: 0 });
    const ManualClick = (event, { index }) => setState({ active_element: index });
    return (
        <Router>
            <header>
                <div id="logo"><img src={img_logo} /></div>
                    <ul>
                        <div className="nav" id="nav-panel">
                            <li>
                                <Link to="/xo-panel">
                                    <div className={state.active_element === 0 ? 'active' : ''} onClick={(event) => ManualClick(event, { index: 0 })}> 
                                        Игровое поле
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/top">
                                    <div className={state.active_element === 1 ? 'active' : ''} onClick={(event) => ManualClick(event, { index: 1 })}>
                                        Рейтинг
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/activeplayers">
                                    <div className={state.active_element === 2 ? 'active' : ''} onClick={(event) => ManualClick(event, { index: 2 })}>
                                        Активные игроки
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/history">
                                    <div className={state.active_element === 3 ? 'active' : ''} onClick={(event) => ManualClick(event, { index: 3 })}>
                                        История игр
                                    </div>
                                </Link>
                            </li>
                            <li>
                                <Link to="/listplayers">
                                    <div className={state.active_element === 4 ? 'active' : ''} onClick={(event) => ManualClick(event, { index: 4 })}>
                                        Список игроков
                                    </div>
                                </Link>
                            </li>
                        </div>
                    </ul>
                <button onClick={SessionArchive.processExit}>
                    <img src={img_signout}/>
                </button>
            </header>
            <Routes>
                <Route path="/header" element={<Header/>}></Route>
                <Route path="/xo-panel" element={<Panel_XO/>}></Route>
                <Route path="/top" element={<Top/>}></Route>
                <Route path="/" element={<Panel_XO/>}></Route>
                <Route path="/activeplayers" element={<Active/>}></Route>
                <Route path="/listplayers" element={<List_Players/>}></Route>
                <Route path="/history" element={<History_Games/>}></Route>
            </Routes>
        </Router>
    );
}

export default Header;