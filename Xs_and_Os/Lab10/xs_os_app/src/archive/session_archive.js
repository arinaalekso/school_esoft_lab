import { makeObservable, observable, action } from 'mobx';

class SessionArch 
{
    isAuthoLogin = false;
    ID_session = null;
    constructor(){
        makeObservable(this, {
            isAuthoLogin: observable, 
            ID_session: observable, 
            processEntry : action,
        });
    }
    processEntry() 
    {
        SessionArchive.isAuthoLogin = true;
        SessionArchive.ID_session = 1;
        console.info(SessionArchive.isAuthoLogin);
        localStorage.setItem('isAuthoLogin', SessionArchive.isAuthoLogin);
        localStorage.setItem('ID_session', SessionArchive.ID_session);
        console.info(localStorage.getItem('ID_session'));
    }
    processExit() 
    {
        SessionArchive.isAuthoLogin = false;
        SessionArchive.ID_session = null;
        console.info(SessionArchive.isAuthoLogin);
        localStorage.setItem('isAuthoLogin', SessionArchive.isAuthoLogin);
        localStorage.setItem('ID_session', SessionArchive.ID_session);
        document.location.reload();
    }
}

const SessionArchive = new SessionArch();
export default SessionArchive;
