import { makeObservable, observable, action } from 'mobx';

class ModalArch 
{
    showModalWindows = true;
    constructor(){
        makeObservable(this, {
            showModalWindows: observable,
            closeModalWindows: action,});
    }
    closeModalWindows() 
    {
        this.showModalWindows = false;
    }
}

const ModalArchive = new ModalArch();
export default ModalArchive;