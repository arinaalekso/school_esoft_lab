//import React from 'react';
import { makeObservable, observable, action, autorun, reaction, computed } from 'mobx';
import ModalArchive from './modal_archive';  
import ImitatApiGame from '../components/mockup/imitation_games';
import GamesMockUpkWS from '../components/mockup/ws_mockup';
import Players from '../components/players';
import img_x_xxl from "../components/imgs/xxl-x.svg";
import img_zero_xxl from "../components/imgs/xxl-zero.svg";

function GB(props) 
{
    return (
        <div className="cell" onClick={props.onClick}>
            {(props.value === 'X' ? <img src={img_x_xxl}/> : null) }
            {(props.value === 'O' ? <img src={img_zero_xxl}/> : null) }
        </div>);
}

class GameArch 
{
    user_player = [];
    chat_games = [];
    get player_X()
    {
        return this.user_player[0];
    }
    get player_O()
    {
        return this.user_player[1];
    } 
    get FirstMove_X() 
    {
        return this.player_X?.name.split(' ')[0];
    }
    get FirstMove_O() 
    {
        return this.player_O?.name.split(' ')[0];
    }
    get LastMove_X() 
    {
        return this.player_X?.name.split(' ')[1];
    }
    get LastMove_O() 
    {
        return this.player_O?.name.split(' ')[1];
    }
    get FullMove_X() 
    {
        return this.player_X?.name;
    }
    get FullMove_O()
    {
        return this.player_O?.name;
    }
    get Move_X() 
    {
        return this.FirstMove_X +" "+ this.LastMove_X;
    }
    get Move_O()
    {
        return  this.FirstMove_O +" "+ this.LastMove_O;
    }
    get AmountWin_X() 
    {
        return this.player_X?.amountWin * 100 +'% побед'
    }
    get AmountWin_O() 
    {
        return this.player_O?.amountWin * 100 +'% побед'
    }
    get ActualPlayer () 
    {
        if (this.NextMoveX) 
        {
            return this.player_X
        }
        else 
        {
            return this.player_O
        }
    }    

    winner_player = null;
    gameboard = Array(9).fill(null);
    NextMoveX = true;
    cell_value = null;
    seconds = 0;
    isOnRunningTimer = true;
    interval = null;
    isOnRunningGame = true;

    setUsersPlayers(user_player) 
    {
        this.user_player = user_player;
    }
    uploadingInfo() 
    {
        this.setUsersPlayers(ImitatApiGame.getPlayersInfo());
    }
    async uploadingGhat() 
    {
        const chat =  JSON.parse(await ImitatApiGame.getChatMessage());
        if (chat) 
        {
            this.setChatMessages(chat);
        }
    }
    uploadingNextMove() 
    {
        const status_game = ImitatApiGame.getisNextMove();
        if (status_game) 
        {
            if (status_game === "true") 
            {
                this.sendIsNextMove(true);
            } 
            else if (status_game === "false")
            {
                this.sendIsNextMove(false);
            }
        }
    };
    async uploadingField() 
    {
        let gbFromStorage =JSON.parse(await ImitatApiGame.getFieldState()); 
        if (gbFromStorage) 
        {
            this.setGameBoard(gbFromStorage);
        }
    };
    setWinPlayer(winner_player) 
    {
        this.winner_player = winner_player;
        localStorage.setItem('winner_player', winner_player);
    };
    WinnerOrNo(gameboard) 
    {
        const winner_lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],];
        let winner = false;
        let drawngame;
        for (let i = 0; i < winner_lines.length; i++) 
        {
            const [cell_1, cell_2, cell_3] = winner_lines[i];
            if (gameboard[cell_1] && gameboard[cell_1] === gameboard[cell_2] && gameboard[cell_1] === gameboard[cell_3]) 
            {
                this.setWinPlayer(gameboard[cell_1])
                winner = true;
                break;
            }
        }
        if (winner === false) 
        {
            if (!gameboard.includes(null))
            {
                drawngame = 'Ничья'
            }
            this.setWinPlayer(drawngame)
            winner = true;
            return;
        }
        return null;
    }
    
    setGameBoard(newGameBoard) 
    {
        this.gameboard = newGameBoard;
    };
    setPlayerImage(newPlayerImage) 
    {
        this.PlayerImage = newPlayerImage;
    };
    setNextMoveX(NextMoveX) 
    {
        this.NextMoveX = !this.NextMoveX;
    }
    setChatMessages(newChatMessages) 
    {
        this.chat_games = newChatMessages;
        localStorage.setItem('chat_games', JSON.stringify(this.chat_games));
    }
    setIsGameRunning(isRunning) 
    {
        this.isOnRunningGame = isRunning;
    };
    FactoryDefault () 
    {
        console.info('reset state called');
        console.info(GameArchive);
        GameArchive.gameboard = Array(9).fill(null);
        localStorage.setItem('newGameBoard',  JSON.stringify(GameArchive.gameboard));
        GameArchive.NextMoveX = true;
        localStorage.setItem('NextMoveX', GameArchive.NextMoveX);
        GameArchive.winner_player = null;
        localStorage.setItem('winner_player', JSON.stringify(GameArchive.winner_player));
        GameArchive.cell_value = null;
        ModalArchive.closeModalWindows();
        console.log(ModalArchive.showModalWindows);
        ModalArchive.showModalWindows = true;
    }
    renderGB(num) 
    {
        return (<GB value={this.gameboard[num]}
        onClick={() => this.processOnClick(num)}/>    
        );
    }
    get formattedTime() 
    {
        const minutes = Math.floor(this.seconds / 60);
        const remainingsec = this.seconds % 60;
        const formattedTime = `${minutes < 10 ? '0' : ''}${minutes}:${remainingsec < 10 ? '0' : ''}${remainingsec}`;
        return formattedTime;
    }
    StartTimeRunning() 
    {
        this.isOnRunningTimer = true;
        this.interval = setInterval(() => {
            this.seconds += 1;
        }, 1000);
    }
    StopTimeRunning() 
    {
        this.isOnRunningTimer = false;
        clearInterval(this.interval);
    }
    ResetTimeRunning() 
    {
        this.seconds = 0;
        this.isOnRunningTimer = false;
        clearInterval(this.interval);
        this.StartTimeRunning();
    }
    processStopTimer  = () => {this.setIsGameRunning(this.winner_player !== null);};
    processOnClick(num) 
    {
        const row = Math.floor(num / 3);
        const col = num % 3;
        if (this.NextMoveX)
        {
            GamesMockUpkWS.MakeaMove(this.Move_X, row, col, this.gameboard[num]);
        }
        else
        {
            GamesMockUpkWS.MakeaMove(this.Move_O , row, col, this.gameboard[num]);
        }
        if (this.winner_player || this.gameboard[num]) 
        {
            return;
        }
        const newGameBoard = this.gameboard.slice();
        newGameBoard[num] = this.NextMoveX ? 'X' : 'O';
        this.setGameBoard(newGameBoard);
        localStorage.setItem('newGameBoard',  JSON.stringify(GameArchive.gameboard));
        this.setNextMoveX(this.NextMoveX)
        localStorage.setItem('NextMoveX', this.NextMoveX);
        this.WinnerOrNo(newGameBoard);
        this.ResetTimeRunning();
    };
    constructor(){
        if (JSON.parse(localStorage.getItem('newGameBoard')) != undefined)
        {
            this.gameboard=JSON.parse(localStorage.getItem('newGameBoard'));
        }
        else
        {
            this.gameboard=this.gameboard
        }
        if (localStorage.getItem('NextMoveX') == 'false') 
        {
            this.NextMoveX = false
        } 
        
        if (localStorage.getItem('winner_player') === 'X' || localStorage.getItem('winner_player') === 'O' || localStorage.getItem('winner_player') === 'Ничья') 
            this.winner_player = localStorage.getItem('winner_player'); 

        makeObservable(this, {
            winner_player: observable,
            NextMoveX: observable,
            gameboard: observable,
            cell_value: observable,
            chat_games: observable,
            seconds: observable,
            interval: observable,
            setGameBoard: action,
            setChatMessages: action,
            setWinPlayer: action,
            FactoryDefault: action,
            formattedTime: computed,
            StartTimeRunning: action,
            StopTimeRunning: action,
            ResetTimeRunning: action,
            isOnRunningGame: observable,
            setIsGameRunning: action,
            player_O: computed,
            player_X: computed,
            user_player: observable,
            ActualPlayer: computed,
            AmountWin_O: computed,
        })
        autorun((StartTimeRunning) => {this.StartTimeRunning();});
        reaction(() => this.winner_player,() => {
            if (this.winner_player !== null) 
            {
                this.StopTimeRunning();
            }
        });
        reaction(() => this.gameboard, () => {
            if (this.gameboard.includes(null)===false) 
            {
                this.WinnerOrNo();
                this.setIsGameRunning(false);
                this.setModalActive(true);
            }});
        reaction(() => this.gameboard,() => {ImitatApiGame.sendFieldState(this.gameboard)});
        reaction(() => this.chat_games,() => {ImitatApiGame.sendChatMessage(this.chat_games)});
        reaction(() => this.NextMoveX, () => {ImitatApiGame.sendIsNextMove(this.NextMoveX);});
        this.uploadingInfo();
        this.uploadingField();
        this.uploadingNextMove();
        this.uploadingGhat();
    };
}
const GameArchive = new GameArch();
export default GameArchive;