import React from "react"
import {observer} from "mobx-react"
import '../App.css'
import img_logo from './imgs/casual-life-3d-blue-scared-ghost 1.svg'
import SessionArchive from "./xs_os_app/src/archive/session_archive" 

function Auth() {
  const title_text="Только английские буквы, цифры и знаки: . и _";
  return (
    <div className="body_auth">
      <div id="modal">
        <img src={img_logo} />
        <h1 className="head_auth">Войдите в игру</h1>
        <form id="inputs" >
          <input type="text" className="login_text" id="login" name="login" placeholder="Логин" title={title_text} required/>
          <input type="password" className="password_text" id="password" name="password" placeholder="Пароль" title={title_text} required />
          <input type="submit" onClick={SessionArchive.processEntry} className="log_in" id="button_auth" value="Войти" ></input>
          {console.log('SessionArchive.processEntry')}
        </form>
        <script src="/public/javascripts/validation_data.js"></script>
      </div>
    </div>
  );
}

export default observer(Auth);