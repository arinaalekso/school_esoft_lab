const express=require('express');
const router=express.Router();
const acc_db=require('../db/accounts_db');
const top_db=require("../db/top_db");
const PasswordVerificat=require("../middleware/verif_password.js");
const bcrypt=require("bcrypt");

router.get("/main_auth",(req,res)=>{
    res.render("main_auth",
    {
        accs:acc_db,
        bcrypt:bcrypt
    })
})
router.get("/top",(req,res)=>{
    res.render("top",
    {
        top:top_db
    });
})

module.exports=router;