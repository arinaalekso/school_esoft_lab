const bcrypt=require("bcrypt");
const encryptRounds = 10;

const PasswordVerificat=(hash,password)=>{
    return  bcrypt.compare(hash,password);       
}

module.exports=PasswordVerificat;