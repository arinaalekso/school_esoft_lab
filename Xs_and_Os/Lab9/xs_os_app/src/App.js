import './App.css';
import React from "react"
import Header from './components/header';
import img_logo from './components/imgs/casual-life-3d-blue-scared-ghost 1.svg'

class App extends React.Component 
{
  constructor(props) {
    super(props);
    this.state = {isLogin: false}
    this.Log_in = this.Log_in.bind(this);
  }
  Log_in() {
    this.setState({isLogin: true});
  }
  render() {
    if (this.state.isLogin) 
    {
      return (
        <div className="App">
          <Header></Header>
        </div>
      );
    } 
    else 
    {
      return (
        <div className="App">
          <Auth onLogin={this.Log_in}></Auth>
        </div>
      );
    }
  }
}
const title_text="Только английские буквы, цифры и знаки: . и _";

class Auth extends React.Component 
{
  render() {    
    return (
      <div className="main_auth">
        <div id="modal_auth">
          <img src={img_logo} />
          <h1 className="h1_auth">Войдите в игру</h1>
          <form id="form_auth" >
            <input type='text' className="login_text" id="login" name="login" placeholder="Логин"  title={title_text} required/>
            <input type='password' className="password_text" id="password" name="password" placeholder="Пароль" title={title_text} required/>
            <input onClick={this.props.onLogin} type="submit" className="log_in" id="button_auth" value="Войти" /> 
          </form>
          <script src="/src/components/validation_data.js"></script>
        </div>
      </div>
    );
  }
}

export default App;
