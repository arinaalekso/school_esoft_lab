import React, {useState} from 'react';
import img_trophy from './imgs/trophy.svg'
import Panel_XO from './xo-panel'; 

const ModalWindow = (props) => {
  const [showModalWindow, setShowModalWindow] = useState(true);
  const CloseModalWindow = () => {
    setShowModalWindow(false);
  };
  if (!showModalWindow) 
  {
    return null;
  }
  return (
    <div className='modal_back'>
      <div className="popup_modalwindow">
        {props.children}
        <img src={img_trophy}/>
        <h2>{props.user_winner} победил!</h2>
        <button className='open_newgame' onClick={props.resetState} style={{backgroundColor: '#60C2AA'}}>Новая игра</button>
        <button className='close_modalwin' onClick={CloseModalWindow} style={{backgroundColor: '#F7F7F7', color: '#373745'}}>Закрыть окно</button>
      </div>
    </div>
  );
};

export default ModalWindow;