const Players = [
    { 
      id:1, 
      personalname: 'Александров Игнат Анатолиевич', 
      age: 24,  gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'},
    { 
      id:2, 
      personalname: 'Мартынов Остап Фёдорович', 
      age: 12,  
      gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:3, 
      personalname: 'Комаров Цефас Александрович', 
      age: 83,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:4, 
      personalname: 'Кулаков Станислав Петрович', 
      age: 43,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:5, 
      personalname: 'Борисов Йошка Васильевич', 
      age: 32,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:6, 
      personalname: 'Негода Михаил Эдуардович', 
      age: 33,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:7, 
      personalname: 'Жданов Зураб Алексеевич', 
      age: 24,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    { 
      id:8, 
      personalname: 'Бобров Фёдор Викторович', 
      age: 19,  
      gender: 'male',  
      blocked: false,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
    {
      id:9, 
      personalname: 'Многогрешный Павел Виталиевич', 
      age: 24,  
      gender: 'male',  
      blocked: true,  
      create: '12 октября 2021',  
      changed: '22 октября 2021'
    },
  ]  

export default Players;