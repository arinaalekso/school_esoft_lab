import React, { useState } from "react";
import "../App.css";
import img_male from "./imgs/male.svg";
import img_female from "./imgs/female.svg";
import Players from "./players";
import Status_Block from "./status_block";
import Button_Block from "./button_blocked";
import Add_Player from "./add_player";

const ListPlayers = (props) => {
  const [players, setPlayers] = useState(Players);
  const [showAddPlayer, setShowAddPlayer] = useState(false);
  const ProcessBlockingStatus = (playerId) => {
    const UpdatInfoPlayers = players.map((player) => player.id === playerId ? { ...player, blocked: !player.blocked } : player
    );
    setPlayers(UpdatInfoPlayers);
  };
  const ProcessAddPlayer = (newPlayer) => {
    setPlayers([...players, newPlayer]);
    setShowAddPlayer(false);
  };
  const ProcessShowAddPlayer = () => {
    setShowAddPlayer(!showAddPlayer);
  };
  const ProcessCloseAddPlayer = () => {
    setShowAddPlayer(false);
  };
  return (
    <div id="list_players">
      <div id="active_header" style={{width: '100%', height: '48px'}}>
        <h2>Список игроков</h2>
        {showAddPlayer ? <Add_Player onClose={ProcessCloseAddPlayer} onAddPlayer={ProcessAddPlayer}/> : null}
        <button className='mainbutton' style={{height: '48px', width:'173px'}} onClick={ProcessShowAddPlayer}>Добавить игрока</button>
      </div>
      <div id="tableof_activeplayers">
        <div className="header_listplayers">
          <th className="first_column">ФИО</th>
          <th className="middle_columns" style={{ width: "108px" }}>
            Возраст
          </th>
          <th className="half_columns">Пол</th>
          <th className="middle_columns" style={{ width: "152px" }}>
            Статус
          </th>
          <th className="middle_columns">Создан</th>
          <th className="middle_columns">Изменен</th>
          <th className="last_columns"></th>
        </div>
        {players.map((player) => (
            <div className="row_listplayers" key={player.id}>
              <td className="first_col">{player.personalname}</td>
              <td className="middle_col" style={{ width: "108px" }}>
                {player.age}
              </td>
              <td className="half_col">
                {player.gender === "male" ? <img src={img_male} /> : <img src={img_female} />}
              </td>
              <td className="middle_col">
                <Status_Block blocked={player.blocked} />
              </td>
              <td className="middle_col">{player.create}</td>
              <td className="middle_col">{player.changed}</td>
              <Button_Block
                blocked={player.blocked}
                onStatusChang={() => ProcessBlockingStatus(player.id)}
              />
            </div>
        ))}
      </div>
    </div>
  );
};

export default ListPlayers;