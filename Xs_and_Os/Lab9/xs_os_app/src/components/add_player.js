import React, { useState } from 'react';
import close from './imgs/close.svg';
import img_male from "./imgs/male.svg";
import img_female from "./imgs/female.svg";
import Players from './players';

const AddPlayer = (props) => {
  const [personalname, setPersonalName] = useState('');
  const [age, setAge] = useState('');
  const [gender, setGender] = useState('male');
  const [showModal, setShowModal] = useState(true);
  const ChangingUserName = (event) => {setPersonalName(event.target.value);};
  const ChangingUserAge = (event) =>{setAge(event.target.value);};
  const ChangingUserGender = (event) => {setGender(event.target.value);};  
  const SendingSubmit = (event) => 
  {
    event.preventDefault();
    const NewPlayer = {
      id: Players.length+1, 
      personalname,
      age,
      gender,
      blocked: false,
      create: new Date().toLocaleDateString(),
      changed: new Date().toLocaleDateString()
    };
    props.onAddPlayer(NewPlayer);
    setPersonalName('');
    setAge('');
    setGender('male'); 
  };
  const CloseModal = () => {
    setShowModal(false);
    props.onClose();
  };
  if (!showModal) 
  {
    return null;
  }
  return (  
    <div className='addmodal_back'>
      <form className='addplayer_modal' onSubmit={SendingSubmit}>
        <button className='close_row' onClick={CloseModal}>
          <img src={close}/>
        </button>
        <h2>Добавьте игрока</h2>
        <div className='dataset'>
          ФИО
          <input className='input_field_text' type="text" placeholder="Иванов Иван Иванович" value={personalname} onChange={ChangingUserName} required />
        </div>
        <div className='row_selection'>
          <div className='dataset'> 
            Возраст
            <input className='input_field_text' style={{width: '75px', textAlign: 'center' }} type="number" min="0" placeholder="0" value={age} onChange={ChangingUserAge} required />
          </div> 
          <div className='dataset'> 
            Пол
            <div className='set_radiobtn_gender'>
              <label>
                <input type="radio" value="female" checked={gender === 'female'} onChange={ChangingUserGender} style={{display: 'none'}} />
                <div className={gender === 'female' ? 'radiobtn_checked' : 'radiobtn' } >
                  <img src={img_female} alt="Woman" style={{height: '35px', width:'35px'}}/>
                </div>
              </label>
              <label>
                <input type="radio" value="male" checked={gender === 'male'} onChange={ChangingUserGender} style={{display: 'none'}} />
                <div className={gender === 'male' ? 'radiobtn_checked' : 'radiobtn' } >
                  <img src={img_male} alt="Men" style={{height: '35px', width:'35px'}}/>
                </div>
              </label>         
            </div>
          </div>
        </div>
        <button type="submit" className='add'>Добавить</button>
      </form>
    </div>
  );
};

export default AddPlayer;