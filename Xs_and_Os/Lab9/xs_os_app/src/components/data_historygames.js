const Data_Games = 
[
  { 
    id:1, 
    player_O: 'Терещенко У.Р.', 
    player_X: 'Многогрешный П.В.',  
    winner: 'O', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:2, 
    player_O: 'Горбачёв А. Д.', 
    player_X: 'Многогрешный П.В.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:3, 
    player_O: 'Константинов В. Н.',
    player_X: 'Сасько Ц. А.',  
    winner: 'O', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:4, 
    player_O: 'Никифорова Б. А.', 
    player_X: 'Горбачёв А. Д.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:5, 
    player_O: 'Кулишенко К. И.', 
    player_X: 'Вишняков Ч. М.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  {
    id:6, 
    player_O: 'Гриневска Д. Б.', 
    player_X: 'Кудрявцев Э. В.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:7, 
    player_O: 'Нестеров Х. А.', 
    player_X: 'Исаева О. А.',  
    winner: 'O', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:8, 
    player_O: 'Исаева О. А.', 
    player_X: 'Кулишенко К. И.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:9, 
    player_O: 'Коновалова В. В.', 
    player_X: 'Терещенко У. Р.',  
    winner: 'X', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:10, 
    player_O: 'Казаков Х. Е.', 
    player_X: 'Овчаренко Б. М.',  
    winner: 'O', 
    date_game: '12 февраля 2022', 
    time_game: '43 мин 13 сек'
  },
  { 
    id:11, 
    player_O: 'Сасько Ц. А.', 
    player_X: 'Никифорова Б. А.',  
    winner: 'X', 
    date_game: '12 февраля 2022',
    time_game: '43 мин 13 сек'
  },       
]

export default Data_Games;