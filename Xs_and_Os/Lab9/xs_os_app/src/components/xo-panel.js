import React from "react"
import '../App.css'
import img_x from "./imgs/x.svg"
import img_zero from "./imgs/zero.svg"
import img_xxl_x from "./imgs/xxl-x.svg"
import img_xxl_o from "./imgs/xxl-zero.svg"
import img_send_btn from "./imgs/send-btn.svg"
import ModalWindow from "./modal"

function SquareWinner(props) 
{
    return (
        <div className="cell" onClick={props.onClick}>
            {props.value}
        </div>
    );
}

function WinnerOrNo(win_squares) 
{
    const winner_lines = 
    [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < winner_lines.length; i++) 
    {
        const [cell_1, cell_2, cell_3] = winner_lines[i];
        if (win_squares[cell_1] && win_squares[cell_1] === win_squares[cell_2] && win_squares[cell_1] === win_squares[cell_3]) 
        {
            return win_squares[cell_1];
        }
    }
    return null;
}

class XOpanel extends React.Component 
{
    constructor(props) 
    {
        super(props);
        this.state = 
        {
            quadrate: Array(9).fill(null),
            Next: true,
            ImgPlayer: Array(9).fill(null),
        }
    }
    InitialState = () => 
    {
        this.setState({
            quadrate: Array(9).fill(null),
            Next: true,
            ImgPlayer: Array(9).fill(null),
        })
    }
    ClickCell(num) 
    {
        const quadrate = this.state.quadrate.slice();
        const ImgPlayer = this.state.ImgPlayer.slice();
        if (WinnerOrNo(quadrate) || quadrate[num]) 
        {
            return;
        }
        quadrate[num] = this.state.Next ? 'X': 'O';
        ImgPlayer[num] = this.state.Next ? <img src={img_xxl_x} /> : <img src={img_xxl_o} />
        this.setState({
            quadrate: quadrate, 
            Next: !this.state.Next,
            ImgPlayer: ImgPlayer,
        })
    }
    
    DisplaySquare(num) 
    {
        return (<SquareWinner value={this.state.ImgPlayer[num]} onClick={() => this.ClickCell(num)}/>);
    }

    render() {
        const winner = WinnerOrNo(this.state.quadrate);
        let status_winner;
        if (winner) 
        {
            status_winner = 
            <div id="game-step">Победитель: &nbsp;{(winner === 'X' ? <img src={img_x}/>: 
                <img src={img_zero}/>)} &nbsp;{(this.state.IsNext ? 'Владелен Пупкин' : 'Екатерина Плюшкина' )}
            </div>;
            let user_winner = (winner === 'X' ? 'Екатерина Плюшкина' : 'Владелен Пупкин' )
            return(
                <div id="main-container">
                    <div id="subject-list">
                        <h1>Игроки</h1>
                        <div class="container">
                            <div class="subject-container">
                                <div><img class="subject-icon" src={img_zero} /></div>
                                <div class="subject-info">
                                    <div class="subject-name">Пупкин Владелен Игоревич</div>
                                    <div class="subject-percent">63% побед</div>
                                </div>
                            </div>
                            <div class="subject-container">
                                <div><img class="subject-icon" src={img_x} /></div>
                                <div class="subject-info">
                                    <div class="subject-name">Плюшкина Екатерина Викторовна</div>
                                    <div class="subject-percent">23% побед</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="game-container">
                        <div id="game-time">05:12</div>
                        <div id="game-board">
                            {this.DisplaySquare(0)}
                            {this.DisplaySquare(1)}
                            {this.DisplaySquare(2)}
                            {this.DisplaySquare(3)}
                            {this.DisplaySquare(4)}
                            {this.DisplaySquare(5)}
                            {this.DisplaySquare(6)}
                            {this.DisplaySquare(7)}
                            {this.DisplaySquare(8)}
                            <ModalWindow user_winner = {user_winner} InitialState={this.InitialState}/>
                        </div>
                        {status_winner}
                    </div>
                    <div id="chat-container">
                        <div class="msgs-container">
                            <div class="msg-container other">
                                <div class="msg-header">
                                    <div class="subject-name x">Плюшкина Екатерина</div>
                                    <div class="time">13:40</div>
                                </div>
                                <div class="msg-body">Ну что, готовься к поражению!!1</div>
                            </div>
                            <div class="msg-container me">
                                <div class="msg-header">
                                    <div class="subject-name zero">Пупкин Владлен</div>
                                    <div class="time">13:41</div>
                                </div>
                                <div class="msg-body">Надо было играть за крестики. Розовый — мой не самый счастливый цвет</div>
                            </div>
                            <div class="msg-container me">
                                <div class="msg-header">
                                    <div class="subject-name zero">Пупкин Владлен</div>
                                    <div class="time">13:45</div>
                                </div>
                                <div class="msg-body">Я туплю..</div>
                            </div>
                            <div class="msg-container other">
                                <div class="msg-header">
                                    <div class="subject-name x">Плюшкина Екатерина</div>
                                    <div class="time">13:47</div>
                                </div>
                                <div class="msg-body">Отойду пока кофе попить, напиши в тг как сходишь</div>
                            </div>
                        </div>
                        <div class="msg-interactive-elements">
                            <textarea placeholder="Сообщение.."></textarea>
                            <button><img src={img_send_btn}/></button>
                        </div>
                    </div>
                </div>
            )
        } 
        else 
        {
            status_winner = <div id="game-step">Ходит&nbsp;{(this.state.Next ? <img src={img_x}/>: <img src={img_zero}/>)} &nbsp;{(this.state.Next ? 'Екатерина Плюшкина':'Владелен Пупкин' )}</div>;
            return(
                <div id="main-container">
                    <div id="subject-list">
                        <h1>Игроки</h1>
                        <div class="container">
                            <div class="subject-container">
                                <div><img class="subject-icon" src={img_zero} /></div>
                                <div class="subject-info">
                                    <div class="subject-name">Пупкин Владелен Игоревич</div>
                                    <div class="subject-percent">63% побед</div>
                                </div>
                            </div>
                            <div class="subject-container">
                                <div><img class="subject-icon" src={img_x} /></div>
                                <div class="subject-info">
                                    <div class="subject-name">Плюшкина Екатерина Викторовна</div>
                                    <div class="subject-percent">23% побед</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="game-container">
                        <div id="game-time">05:12</div>
                        <div id="game-board">
                            {this.DisplaySquare(0)}
                            {this.DisplaySquare(1)}
                            {this.DisplaySquare(2)}
                            {this.DisplaySquare(3)}
                            {this.DisplaySquare(4)}
                            {this.DisplaySquare(5)}
                            {this.DisplaySquare(6)}
                            {this.DisplaySquare(7)}
                            {this.DisplaySquare(8)}
                        </div>
                        {status_winner}
                    </div>
                    <div id="chat-container">
                        <div class="msgs-container">
                            <div class="msg-container other">
                                <div class="msg-header">
                                    <div class="subject-name x">Плюшкина Екатерина</div>
                                    <div class="time">13:40</div>
                                </div>
                                <div class="msg-body">Ну что, готовься к поражению!!1</div>
                            </div>
                            <div class="msg-container me">
                                <div class="msg-header">
                                    <div class="subject-name zero">Пупкин Владлен</div>
                                    <div class="time">13:41</div>
                                </div>
                                <div class="msg-body">Надо было играть за крестики. Розовый — мой не самый счастливый цвет</div>
                            </div>
                            <div class="msg-container me">
                                <div class="msg-header">
                                    <div class="subject-name zero">Пупкин Владлен</div>
                                    <div class="time">13:45</div>
                                </div>
                                <div class="msg-body">Я туплю..</div>
                            </div>
                            <div class="msg-container other">
                                <div class="msg-header">
                                    <div class="subject-name x">Плюшкина Екатерина</div>
                                    <div class="time">13:47</div>
                                </div>
                                <div class="msg-body">Отойду пока кофе попить, напиши в тг как сходишь</div>
                            </div>
                        </div>
                        <div class="msg-interactive-elements">
                            <textarea placeholder="Сообщение.."></textarea>
                            <button><img src={img_send_btn}/></button>
                        </div>
                    </div>
                </div>
            )
        }    
    }
}

export default XOpanel;