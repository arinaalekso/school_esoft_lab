import React, {useState} from 'react';
import MainButton from './main_button';
import CellStatus from './cell_status';

const RowActiv = (props) => {
  const [borrow, setBorrow] = useState(true);
  const Process_Status_Changes = () => {setBorrow(!borrow);};
  if (!props.show && !borrow) 
    return (null)
  
  return (
  <div className="activ_row">
    <div className='name_of_activeplayer'>
      {props.player}
    </div>
    <div className='block_right'>
      <CellStatus borrow={borrow} onStatusChang={Process_Status_Changes}/>
      <MainButton borrow={borrow} call={borrow}/>
    </div>      
  </div>
  );
};

export default RowActiv;