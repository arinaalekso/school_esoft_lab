import React, { useState } from 'react';

const CellStatus = (props) => {
  const [borrow, setBorrow] = useState(props.borrow);
  const Process_Status_Changes = () => {
    setBorrow(!borrow);
    props.onStatusChang();
  };
  if (!borrow) 
  {
    return (
      <div className="status_borrow">
        {props.children}
        <button onClick={Process_Status_Changes}>В игре</button>
      </div>
    )
  }  
  return (
    <div className="status_freedom">
      {props.children}
      <button onClick={Process_Status_Changes}>Свободен</button>
    </div>
  );
};

export default CellStatus;