import React, {useState} from 'react';

const MainButton = (props) => { 
  if (!props.borrow) 
  {
    return (
      <div className="additionalbutton">
        {props.children}
        <button>Позвать играть</button>
      </div>
    )    
  }
  return (
    <div className="mainbutton">
      {props.children}
      <button>Позвать играть</button>
    </div>
  );
};

export default MainButton;