import React from "react"
import '../App.css'
import img_logo from './imgs/casual-life-3d-blue-scared-ghost 1.svg'
import App from "../App";

function Auth() 
{
  const title_text="Только английские буквы, цифры и знаки: . и _";
    return (
      <div className="main_auth">
        <div id="modal_auth">
          <img src={img_logo} />
          <h1 className="h1_auth">Войдите в игру</h1>
          <form id="form_auth" >
            <input type="text" className="login_text" id="login" name="login" placeholder="Логин" title={title_text} required/>
            <input type="password" className="password_text" id="password" name="password" placeholder="Пароль" title={title_text} required />
            <input onSubmit={<App handleLogin />} type="submit" className="log_in" id="button_auth" value="Войти"></input>
          </form>
          <script src="/public/javascripts/validation_data.js"></script>
        </div>
      </div>
      

    );
  }
  export default Auth;

