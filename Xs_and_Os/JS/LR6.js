class GameField 
{
    state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
            ];
    mode = 'X';
    queue = 0;
    isOverGame = false;
    winner_O = false;
    winner_X = false;
    
    setMode() //метод для изменения активного игрока
    {
        if (this.mode == "X") 
        {
            this.mode = "O";
        }
        else 
        {
            this.mode = "X";
        }
    };

    getGameFieldStatus()// метод для получения информации о состоянии игрового поля
    {
        let rows = " ";
        for (let i = 0; i < 3; i ++) 
        {
            for (let j = 0; j < 3; j ++) 
            {
                if (gameField.state[i][j] === null) 
                {
                    rows += "  - ";
                } 
                else 
                {
                    rows += " " + gameField.state[i][j] + "  ";
                }
            }
            rows += "\n "
        }
        return rows;
    };   

    FieldCellValue(_column, _row)//метод для заполнения клетки поле  
    {
        if ((_column <= 2) && (_column >= 0) && (_row <= 2) && (_row >= 0))
        {
                if (this.mode === "X") 
                {
                    this.state[_row][_column] = "X";
                    this.setMode();
                    this.queue ++;                    
                }
                else 
                {
                    this.state[_row][_column] = "O";
                    this.setMode();
                    this.queue ++;  
                } 
        }
        else 
        {
            alert("Поле с данными координатами занято или Вы вели не правильные данные. Введите другие координаты.");
        }

    }; 

    Winner(index_row,index_column)//Проверка игрового поле на победителя 
    {
        if (gameField.state[index_row][index_column] === "X") 
        {
            this.winner_X = true; 
        }
        if (gameField.state[index_row][index_column] === "O") 
        {
            this.winner_O = true; 
        }
    };

    СheckingPlayingField()//Проверка игрового поле на логику и победителя по занятым ячейкам и вывод информации о победы
    {
        for (let i = 0; i < 3; i ++) 
        {
            for (let j = 0; j < 3; j ++) 
            {
                if (i === 1) 
                {
                    if ((this.state[i-1][j] === this.state[i][j]) & (this.state[i-1][j] === this.state[i+1][j])) 
                    {
                        this.Winner(i,j);
                    }                
                    if (i === 1 & j === 1) 
                    {
                        if ((this.state[i-1][j-1] === this.state[i][j]) & (this.state[i-1][j-1] === this.state[i+1][j+1])) 
                        {
                            this.Winner(i,j) 
                        }
                        if ((this.state[i][j] === this.state[i-1][j+1]) & (this.state[i][j] === this.state[i+1][j-1])) 
                        {
                            this.Winner(i,j) 
                        }
                    } 
                }
                if (j === 1)  
                {
                    if ((this.state[i][j-1] === this.state[i][j]) & (this.state[i][j-1] === this.state[i][j+1]))
                    {
                        this.Winner(i,j);
                    }
                }                
            }        
        } 

        var winner_info=" ";
        if (this.winner_X == true) 
        {
            winner_info="Крестики победили!";
            this.isOverGame = true;
        }
        else if (this.winner_O == true) 
        {
            winner_info="Нолики победили!";
            this.isOverGame = true;
        }
        else if (this.queue == 9) 
        {
            winner_info="Ничья.";
            this.isOverGame = true;
        }
        if (this.isOverGame)
        {
            alert(winner_info+"\nИгровое поле:\n" + gameField.getGameFieldStatus());    
            console.log(winner_info+"\nИгровое поле:\n" + gameField.getGameFieldStatus());
        }
    };
}

const gameField = new GameField();
while (!gameField.isOverGame) 
{
    console.log("Ходит игрок, играющий "+gameField.mode+".\nИгровое поле:\n"+gameField.getGameFieldStatus());
    let modelwind = prompt("Ходит игрок, играющий "+gameField.mode+".\nВведите координаты через запятую. Например:1,1.\nИгровое поле:\n" + gameField.getGameFieldStatus());
    let index_player = modelwind.split(",");
    gameField.FieldCellValue(index_player[0]-1,index_player[1]-1);
    gameField.getGameFieldStatus();
    gameField.СheckingPlayingField();
};