class GameField
{
    state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
            ]; 
    mode = 'X';
    queue = 0;
    isOverGame = false;
    winner_O = false;
    winner_X = false;
    img_field ='src="../imgs/xxl-x.svg"';
    mode_motion='X';
    img_motion ='src="../imgs/x.svg"';
    username_motion = "Екатерина Плюшкина";

    setMode() //метод для изменения активного игрока
    {
        if (this.mode == "X") 
        {
            this.mode_motion="X";
            this.mode = "O";
            this.username_motion = "Владелен Пупкин"; 
            this.img_motion = 'src="../imgs/zero.svg"';
            this.img_field = 'src="../imgs/xxl-zero.svg"';
        }
        else 
        {
            this.mode_motion="O";
            this.mode = "X";
            this.username_motion = "Екатерина Плюшкина";
            this.img_motion ='src="../imgs/x.svg"';
            this.img_field ='src="../imgs/xxl-x.svg"';
        }
    }; 
    
    Winner(index_row,index_column)//Проверка игрового поле на победителя 
    {
        if (gameField.state[index_row][index_column] === "X") 
        {
            this.winner_X = true; 
        }
        if (gameField.state[index_row][index_column] === "O") 
        {
            this.winner_O = true; 
        }
    };

    СheckingPlayingField()//Проверка игрового поле на логику и победителя по занятым ячейкам и вывод информации о победы
    {
        for (let i = 0; i < 3; i ++) 
        {
            for (let j = 0; j < 3; j ++) 
            {
                if (i === 1) 
                {
                    if ((this.state[i-1][j] === this.state[i][j]) & (this.state[i-1][j] === this.state[i+1][j])) 
                    {
                        this.Winner(i,j);
                    }                
                    if (i === 1 & j === 1) 
                    {
                        if ((this.state[i-1][j-1] === this.state[i][j]) & (this.state[i-1][j-1] === this.state[i+1][j+1])) 
                        {
                            this.Winner(i,j) 
                        }
                        if ((this.state[i][j] === this.state[i-1][j+1]) & (this.state[i][j] === this.state[i+1][j-1])) 
                        {
                            this.Winner(i,j) 
                        }
                    } 
                }
                if (j === 1)  
                {
                    if ((this.state[i][j-1] === this.state[i][j]) & (this.state[i][j-1] === this.state[i][j+1]))
                    {
                        this.Winner(i,j);
                    }
                }                
            }        
        } 

        var winner_info=" ";
        if (this.winner_X == true) 
        {
            winner_info="Крестики победили!";
            this.isOverGame = true;
        }
        else if (this.winner_O == true) 
        {
            winner_info="Нолики победили!";
            this.isOverGame = true;
        }
        else if (this.queue == 9) 
        {
            winner_info="Ничья.";
            this.isOverGame = true;
        }
        if (this.isOverGame)
        {
            alert(winner_info); 
        }
    };
}

const gameField = new GameField();
let info_motion = document.getElementById('game-step');
let cell = document.querySelectorAll('.cell');
let index=''; 

cell.forEach((c) => c.addEventListener('click', () => 
{ 
    if (!c.innerHTML && !gameField.isOverGame) 
    {
        c.innerHTML = `<img ${gameField.img_field}/>`;
        gameField.setMode(gameField.mode);   
        index=(c.getAttribute('id')).replace('cell',''); 
        if (index<=2)
        {
            gameField.state[0][index]=gameField.mode_motion;  
        }
        else if ((index>=3) &&(index<=5))
        {
            gameField.state[1][index-3]=gameField.mode_motion;   
        }
        else
        {
            gameField.state[2][index-6]=gameField.mode_motion;             
        }
        console.log(gameField.state);
        gameField.queue++;
        info_motion.innerHTML = `Ходит&nbsp;<img ${gameField.img_motion} />` + `&nbsp;${gameField.username_motion}`; 
    }
    gameField.СheckingPlayingField();   
})
); 