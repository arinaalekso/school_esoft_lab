const gameFeld = [
    ['x', 'o', 'x'],
    ['x', 'x', 'o'],
    ['x', 'o', 'o']
];
let winner_X = null;
let winner_O = null;

function winnerO_Status (arr) 
{
    const cell_1 = arr[0]; 
    const cell_2 = arr[1]; 
    const cell_3 = arr[2]; 
    const cell_4 = arr[3]; 
    const cell_5 = arr[4]; 
    const cell_6 = arr[5]; 
    const cell_7 = arr[6]; 
    const cell_8 = arr[7]; 
    const cell_9 = arr[8];
    
    if (((cell_1 == 'o' && cell_2 == 'o' && cell_3 == 'o') || (cell_4 == 'o' && cell_5 == 'o' && cell_6 == 'o') || (cell_7 == 'o' && cell_8 == 'o' && cell_9 == 'o')) || ((cell_1 == 'o' && cell_4 == 'o' && cell_7 == 'o') || (cell_2 == 'o' && cell_5 == 'o' && cell_8 == 'o') || (cell_3 == 'o' && cell_6 == 'o' && cell_9 == 'o')) || ((cell_1 == 'o' && cell_5 == 'o' && cell_9 == 'o') || (cell_3 == 'o' && cell_5 == 'o' && cell_7 == 'o'))) 
    {
        winner_O = true;
    }
    else 
    {
        winner_O = false;
    }
    return winner_O;
};

function winnerX_Status (arr) 
{
    const cell_1 = arr[0]; 
    const cell_2 = arr[1]; 
    const cell_3 = arr[2]; 
    const cell_4 = arr[3]; 
    const cell_5 = arr[4]; 
    const cell_6 = arr[5]; 
    const cell_7 = arr[6]; 
    const cell_8 = arr[7]; 
    const cell_9 = arr[8];

    if (((cell_1 == 'x' && cell_2 == 'x' && cell_3 == 'x') || (cell_4 == 'x' && cell_5 == 'x' && cell_6 == 'x') || (cell_7 == 'x' && cell_8 == 'x' && cell_9 == 'x')) || ((cell_1 == 'x' && cell_4 == 'x' && cell_7 == 'x') || (cell_2 == 'x' && cell_5 == 'x' && cell_8 == 'x') || (cell_3 == 'x' && cell_6 == 'x' && cell_9 == 'x')) || ((cell_1 == 'x' && cell_5 == 'x' && cell_9 == 'x') || (cell_3 == 'x' && cell_5 == 'x' && cell_7 == 'x'))) 
    {
        winner_X = true;
    } 
    else
    {
        winner_X = false;
    }
    return winner_X;
};

const win_or_fail_X = (winnerX_Status(gameFeld.flat()));
const win_or_fail_O = (winnerO_Status(gameFeld.flat()));
 
alert(gameFeld[0]+'\n'+gameFeld[1]+'\n'+gameFeld[2]);

if (gameFeld.flat().filter(val => val != null).length !== 9) 
{
    alert('Игра не окончена!(');
} 
else if (win_or_fail_X == true && win_or_fail_O == true) 
{
    alert('Ничья!(');
} 
else if (win_or_fail_O == true) 
{
    alert('Нолики победили!)');
} 
else if (win_or_fail_X == true) 
{
    alert('Крестики победили!)');
}
else 
{
    alert('Ничья!(');
}