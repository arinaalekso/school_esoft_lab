verificare_login=function ()
{
    login = document.getElementsByName('login')[0];
    login.value = login.value.replace(/[^\d|a-z|.|_|A-Z]/g, "");
};

verificare_password=function ()
{
    password = document.getElementsByName('password')[0];    
    password.value = password.value.replace(/[^\d|a-z|.|_|A-Z]/g, "");
};

document.querySelector('input[name=login]').oninput=verificare_login;
document.querySelector('input[name=password]').oninput = verificare_password;

function Checking(form) 
{ 
    function ErrorInfo(input_text, info) 
    {
        const parent_element = input_text.parentNode;
        const error_label = document.createElement('label');
        error_label.classList.add('error-label');
        error_label.textContent = info;
        parent_element.classList.add('error');
        parent_element.append(error_label);
    }

    function DeleteErrorInfo(input_text) 
    {
        const parent_element = input_text.parentNode;
        if (parent_element.classList.contains('error')) 
        {
            parent_element.querySelector('.error-label').remove();
            parent_element.classList.remove('error');
        }
    }
    let result = true;
    const inputs = form.querySelectorAll('input');

    for (const i of inputs) 
    {
        DeleteErrorInfo(i);
        if (i.dataset.required == "true") 
        {
            if (i.value == "") 
            {
                DeleteErrorInfo(i);
                ErrorInfo(i, 'Поле не заполнено!');
                result = false;
            }
        }
    }
    return result;
}

document.getElementById('forma_open').addEventListener('submit', function(event) 
{
    event.preventDefault();
    if (Checking (this) == true) 
    {
        console.log("Логин: "+login.value);
        console.log("Пароль: "+password.value);
        alert("Добро пожаловать, "+login.value+"!");        
    }
})