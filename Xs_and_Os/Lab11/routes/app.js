const express=require('express');
const router=express.Router();
const acc_db=require('../db/accounts_db'); 
const top_db=require('../db/top_db')
const bcrypt=require("bcrypt");
const controllers_users = require('../controllers/controllers_users')
const { protect } = require('../middleware/verif_password')

const {
    create_field,
    getfield_byId,
    updatefield_byId,
    deletefield_byId,
  } = require('../controllers/controllers_game') 
router
  .route('/')
  .post(create_field)
  .get(getfield_byId)
  .put(updatefield_byId)
  .delete(deletefield_byId)

router.get("/main_auth",(req,res)=>{
    res.render("main_auth",
    {
        accs:acc_db,
        bcrypt:bcrypt
    })
}) 
router.get("/top",(req,res)=>{
    res.render("top",
    {
        top:top_db
    });
})
router.get("/game",(req,res)=>{
    res.render("game")
});



module.exports=router;