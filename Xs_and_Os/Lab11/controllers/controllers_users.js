const players = require('../db/top_db')

const getUsers = (req, res) => {
  if (!players.length) {
    res.status(404)
    throw new Error('Информация об игроках отсутсвует')
  }
  res.status(200).json(players)
}

module.exports = {
  getUsers,
}
