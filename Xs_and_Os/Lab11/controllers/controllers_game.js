const fs = require('fs')
const path = require('path')
const info_games = require('../db/history_game.json')

const pathName = path.join(
    path.dirname(require.main.filename),
    'db',
    'history_game.json'
    )
    const saveData = (data) => {
    fs.writeFile(pathName, JSON.stringify(data), (err) => {
        console.log(err)
    })
}

const create_field = (req, res) => {
    const { field } = req.body

    if (!field) 
    {
        res.status(405)
        throw new Error('Параметры запроса не соответсвуют схеме')
    }
    info_games.push(field)
    saveData(info_games)
    res.status(200).json({ message: 'Успешно' })
}
const getfield_byId = (req, res) => {
    const { fieldIdx } = req.query
    if (!info_games[fieldIdx]) 
    {
        res.status(404)
        throw new Error(' Поле c данным индексом отсутсвует')
    }
    const field = info_games[fieldIdx]
    res.status(200).json(field)
}

const updatefield_byId = (req, res) => {
    const { fieldIdx } = req.query
    const { updatedField } = req.body

    if (!info_games[fieldIdx]) 
    {
        res.status(404)
        throw new Error(' Поле c данным индексом нету')
    }

    const info_games_updated = [...info_games]
    info_games_updated[fieldIdx] = updatedField
    saveData(info_games_updated)

    res.status(200).json({ message: 'Успешно' })
}

const deletefield_byId = (req, res) => {
    const { fieldIdx } = req.query
    if (!info_games[fieldIdx]) 
    {
        res.status(404)
        throw new Error(' Поле c данным индексом нету')
    }

    const info_games_updated = info_games.filter((el) => el !== info_games[fieldIdx])
    saveData(info_games_updated)
    res.status(200).json({ message: 'Успешно' })
}

module.exports = {
    create_field,
    getfield_byId,
    updatefield_byId,
    deletefield_byId,
}
